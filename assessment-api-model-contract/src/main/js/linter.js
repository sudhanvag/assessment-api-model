'use strict';

const argv = require('minimist')(process.argv.slice(2));
const fs = require('fs');
const yaml = require('js-yaml');
const loader = require('speccy/lib/loader');
const lint = require('speccy/lint');
const options = {
    resolve: true,
    jsonSchema: true
};
const contractFile = argv.contractFile;

// Load and combine files
loader.loadSpec(contractFile, options)
    .then(spec => {
        fs.writeFileSync(contractFile, yaml.safeDump(spec));
    })
    .catch(err => {
        console.log(err);
        process.exit(1);
    });

// verify rules https://speccy.io/rules/1-rulesets
lint.command(contractFile, {})
    .then(results => {
        if(Array.isArray(results) && results.length) {
            console.log(JSON.stringify(results, null, 4));
            process.exit(1);
        }
    })
    .catch(error => {
        console.log(error);
        process.exit(1);
    });

